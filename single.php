<?php 

get_header(); ?>
  
          <div class="page-in">
        <div class="container">
          <div class="row">
            <div class="col-lg-6 pull-left"><div class="page-in-name"><?php the_title(); ?></div></div>
            <div class="col-lg-6 pull-right"><div class="page-in-bread"><?php if ( function_exists('yoast_breadcrumb') ) {
yoast_breadcrumb('<p id="breadcrumbs">','</p>');
} ?></div></div>
          </div>
        </div>
      
      <div class="container marg50">
        <div class="row">
          <div class="col-lg-12">
            <div class="row">
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

<div class="medium-blog">
<!--
                <div class="col-lg-5">
                  <div class="cl-blog-img"><?php the_post_thumbnail('blog-image');?></div>
                </div>-->
                <div class="col-lg-12">
                  <div class="med-blog-naz">
                    <div class="cl-blog-type"><i class="icon-pencil"></i></div>
                    <div class="cl-blog-name"><?php the_title();?></div>
                    <div class="cl-blog-detail"><?php the_date()?> - <a href="https://plus.google.com/u/0/117371587720581418238?rel=author">Rogier Lankhorst</a> <div class="addthis_sharing_toolbox"></div></div>
                    <div class="cl-blog-text"><?php echo get_custom_content();?></div>
                  </div>
              
                </div>
                <div class="cl-blog-line"></div>
                <?php 
      

        comments_template();
         
      
    ?>
              </div>

		

		<?php
	endwhile;
	else:
  		echo "<p>geen resultaten gevonden</p>";
	endif;
				   
?>


        
          </div> 
        </div>
      </div></div></div>
<?php get_footer();?>