<?php
/*
	Template Name: Portfolio
*/
?>
<?php get_header(); 
?>

      <div class="page-in">
        <div class="container">
          <div class="row">
            <div class="col-lg-6 pull-left"><div class="page-in-name">
<?php the_title(); ?>: <span><?php echo get_post_meta(get_the_ID(), 'ex_ondertitel', true);?></span>
</div></div>
            <div class="col-lg-6 pull-right"><div class="page-in-bread"><?php if ( function_exists('yoast_breadcrumb') ) {
yoast_breadcrumb('<p id="breadcrumbs">','</p>');
} ?></div></div>
          </div>
        </div>
      </div>
      <div class="container marg50">
        <div class="row">
          <div class="col-lg-12">
            <div id="filters-container-portfolio-2" class="cbp-l-filters-button">
<button data-filter="*" class="cbp-filter-item-active cbp-filter-item">Toon alles<div class="cbp-filter-counter"></div></button>
	<?php
$category_id = get_cat_ID('Portfolio');
$args = array(
  'categories'        => $category_id
);
$tags = get_category_tags($args);




		//$tags = get_tags();
		$html = '<div class="post_tags">';
		foreach ( $tags as $tag ) {
			//$tag_link = get_tag_link( $tag->term_id );
			?>
				<button data-filter=".<?php echo $tag->tag_slug;?>" class="cbp-filter-item">
				<?php echo $tag->tag_name;?><div class="cbp-filter-counter"></div></button>
		<?php } ?>
              
            </div>
          </div>
        </div>
      </div>
      <div class="container marg50">
        <div class="grid hover-3">
          <div class="cbp-l-grid-projects" id="grid-container-portfolio-2">
            <ul>


<?php
$category_id = get_cat_ID('portfolio');
$args = array(
	'post_type' => 'post',
	'cat' => $category_id,
	'orderby' => 'rand'
);

$the_query = new WP_Query( $args );

if ( $the_query->have_posts() ) {
	while ( $the_query->have_posts() ) {
		$the_query->the_post();
		?>

	<li class="cbp-item <?php 
	//create some tags
	$posttags = get_the_tags();
	if ($posttags) {
  		foreach($posttags as $tag) {
    			echo $tag->slug." "; 
  			}
		}
	?> design html">
                <div class="portfolio-main">
                  <figure>
                    <?php the_post_thumbnail('portfolio-image-big');?>
                    <figcaption>
                      <h3><?php the_title(); ?></h3>
		<?php
		if (has_tag( 'websitefotografie' )) { ?>
                      <span>Fotografie: Geert Job Sevink</span>
		<?php } ?>
                      <a href="<?php 
$thumb = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'portfolio-image-big' );
echo $thumb['0']; ?>" class="portfolio-attach cbp-lightbox" data-title=""><i class="icon-search"></i></a>
                      <a target="_blank" href="<?php echo get_the_content();?>" class="portfolio-search"><i class="icon-attach"></i></a>
                    </figcaption>
                  </figure> 
                </div>
              </li>


		

		<?php
	}
 	
} else {
	// no posts found
}
/* Restore original Post Data */
wp_reset_postdata();
				   
?>

              
            </ul>
          </div>
   <!--       <div class="col-lg-12">
            <div class="button-center"><a href="portfolio/loadmore-portfolio.html" class="btn-simple cbp-l-loadMore-button-link">Load Full Portfolio</a></div>
          </div>-->
        </div>  
      </div>
    <script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/assets/js/jquery.cubeportfolio.min.js"></script>
    <script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/assets/js/portfolio-2.js"></script>
  
<?php get_footer();?>