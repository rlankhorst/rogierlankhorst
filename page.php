<?php get_header(); ?>
  <?php if ( have_posts() ) : while ( have_posts() ) : the_post();?>
     <div class="page-in">
        <div class="container">
          <div class="row">
            <div class="col-lg-6 pull-left"><div class="page-in-name"><?php the_title(); ?>: <span><?php echo get_post_meta(get_the_ID(), 'ex_ondertitel', true);?></span></div></div>
            <div class="col-lg-6 pull-right"><div class="page-in-bread"><?php if ( function_exists('yoast_breadcrumb') ) {
yoast_breadcrumb('<p id="breadcrumbs">','</p>');
} ?></div></div>
          </div>
        </div>
      </div>
      <div class="container marg75">
        <div class="row">
          <div class="col-lg-12">
            <div class="promo-block">
              <div class="promo-text">
<?php echo get_post_meta(get_the_ID(), 'ex_promo', true);
?></div>
              <div class="center-line"></div>
            </div>
          </div>
        </div>
      </div>
      <div class="container">
        <div class="row">
          <div class="col-lg-4 col-md-4 col-sm-12">
            <div class="introduction"> 
            <?php if ( has_post_thumbnail() ) {
			the_post_thumbnail('default-page-image');
		} else {
			echo '<img src="'. get_bloginfo( 'stylesheet_directory' ) . '/images/mockup.jpg" />';
		}?>
            </div>
          </div> 
          <div >
            <div class="page-content">
              <p class="about-text"><?php echo get_custom_content();?></p>
          <div class="col-lg-12">
              <h2>Bekijk ook:</h2>
        <?php
              wp_nav_menu( array(
              'theme_location' => 'main_menu',
              'sub_menu' => true
              ) );
        
        $parentpost = get_menu_parent("main_menu");
        if (is_object($parentpost)) {
          $title = $parentpost->post_title;
          $slug = $parentpost->post_name;
          echo "<a href='".$slug."'>".$title."</a>";
        }
?>
</div>
            </div>
          </div> 
        </div>
      </div>
<?php 
	endwhile;
   else: ?>
	geen pagina gevonden, probeer een andere pagina. 
<?php
	endif;
?>
<?php get_footer();?>