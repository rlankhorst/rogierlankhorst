<li data-transition="fade" data-slotamount="7" data-masterspeed="700" >
              <img src="<?php bloginfo('stylesheet_directory'); ?>/images/slider-01-scherp.jpg"  alt=""  data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat">
              <div class="tp-caption largeblackbg sfb"
                data-x="center"
                data-y="95"
                data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                data-speed="500"
                data-start="800"
                data-easing="Back.easeOut"
                data-endspeed="500"
                data-endeasing="Power4.easeIn"
                data-captionhidden="on"
                style="z-index: 4">for your webpresence
              </div>
              <div class="tp-caption largeborder skewfromrightshort"
                data-x="center"
                data-y="200"
                data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                data-speed="500"
                data-start="1100"
                data-easing="Back.easeOut"
                data-endspeed="500"
                data-endeasing="Power4.easeIn"
                data-captionhidden="on"
                style="z-index: 9">
              </div>
              <div class="tp-caption largetext sft"
                data-x="center"
                data-y="250"
                data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                data-speed="500"
                data-start="1400"
                data-easing="Back.easeOut"
                data-endspeed="500"
                data-endeasing="Power4.easeIn"
                data-captionhidden="on"
                style="z-index: 9">
			Alleen een website bouwen is niet meer<br>
			genoeg. Onze websites hebben Google, social<br>
			media en gebruikersvriendelijkheid ingeprogrammeerd. 
              </div>
              <div class="tp-caption largebutton lfb"
                data-x="center"
                data-y="360"
                data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                data-speed="500"
                data-start="1800"
                data-easing="Back.easeOut"
                data-endspeed="500"
                data-endeasing="Power4.easeIn"
                data-captionhidden="on"
                style="z-index: 9"><a href="<?php home_url(); ?>/website-portfolio-groningen">Bekijk voorbeelden</a>
              </div>
            </li>
            <li data-transition="fade" data-slotamount="7" data-masterspeed="700" >
              <img src="<?php bloginfo('stylesheet_directory'); ?>/images/slider-02-scherp.jpg"  alt=""  data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat">
              <div class="tp-caption largeblackbg sfb"
                data-x="30"
                data-y="80"
                data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                data-speed="500"
                data-start="800"
                data-easing="Back.easeOut"
                data-endspeed="500"
                data-endeasing="Power4.easeIn"
                data-captionhidden="on"
                style="z-index: 4">Responsive websites
              </div>
              <div class="tp-caption largetext sft"
                data-x="30"
                data-y="160"
                data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                data-speed="500"
                data-start="1100"
                data-easing="Back.easeOut"
                data-endspeed="500"
                data-endeasing="Power4.easeIn"
                data-captionhidden="on"
                style="z-index: 9;text-align:left;">

		In het mobiele tijdperk moet elke<br> 
		site op elk apparaat bruikbaar zijn. <br>
		Is jouw site al navigeerbaar op een smartphone? 

              </div>
              <div class="tp-caption largebutton lft"
                data-x="30"
                data-y="270"
                data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                data-speed="500"
                data-start="2400"
                data-easing="Back.easeOut"
                data-endspeed="500"
                data-endeasing="Power4.easeIn"
                data-captionhidden="on"
                style="z-index: 9"><a href="<?php home_url(); ?>/website-portfolio-groningen">Bekijk voorbeelden</a>
              </div>
              <div class="tp-caption lfb"
                data-x="730"
                data-y="40"
                data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                data-speed="500"
                data-start="1650"
                data-easing="Back.easeOut"
                data-endspeed="500"
                data-endeasing="Power4.easeIn"
                data-captionhidden="on"
                style="z-index: 9"><img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/imac.png" alt="">
              </div>
              <div class="tp-caption lfb"
                data-x="760"
                data-y="405"
                data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                data-speed="500"
                data-start="1850"
                data-easing="Back.easeOut"
                data-endspeed="500"
                data-endeasing="Power4.easeIn"
                data-captionhidden="on"
                style="z-index: 9"><img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/keyboard.png" alt="">
              </div>
              <div class="tp-caption lfb"
                data-x="1020"
                data-y="405"
                data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                data-speed="500"
                data-start="2000"
                data-easing="Back.easeOut"
                data-endspeed="500"
                data-endeasing="Power4.easeIn"
                data-captionhidden="on"
                style="z-index: 9"><img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/mouse.png" alt="">
              </div>
            </li>

<li data-transition="fade" data-slotamount="7" data-masterspeed="700" >
              <img src="<?php bloginfo('stylesheet_directory'); ?>/images/slider-03-scherp.jpg"  alt=""  data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat">
              <div class="tp-caption lfb"
                data-x="30"
                data-y="40"
                data-speed="500"
                data-start="800"
                data-easing="Back.easeOut"
                data-endspeed="500"
                data-endeasing="Power4.easeIn"
                data-captionhidden="on"
                style="z-index: 9"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/camera-icon.png" alt="">
              </div>
              <div class="tp-caption largeblackbg sfb"
                data-x="550"
                data-y="75"
                data-speed="500"
                data-start="1400"
                data-easing="Back.easeOut"
                data-endspeed="500"
                data-endeasing="Power4.easeIn"
                data-captionhidden="on"
                style="z-index: 9">Websitefotografie
              </div>
              <div class="tp-caption largetext sft"
                data-x="550"
                data-y="160"
                data-speed="500"
                data-start="1600"
                data-easing="Back.easeOut"
                data-endspeed="500"
                data-endeasing="Power4.easeIn"
                data-captionhidden="on"
                style="z-index: 9;text-align:left;">
		Professionele fotografie + doordacht design<br>
		= sprankelende websites. 
              </div>
              <div class="tp-caption largeblackbg sfb"
                data-x="540"
                data-y="250"
                data-speed="500"
                data-start="1800"
                data-easing="Back.easeOut"
                data-endspeed="500"
                data-endeasing="Power4.easeIn"
                data-captionhidden="on"
                style="z-index: 9"><i class="icon-photo"></i>
              </div>
              <div class="tp-caption largeblackbg sfb"
                data-x="625"
                data-y="245"
                data-speed="500"
                data-start="2000"
                data-easing="Back.easeOut"
                data-endspeed="500"
                data-endeasing="Power4.easeIn"
                data-captionhidden="on"
                style="z-index: 9"><i class="glyphicon glyphicon-plus"></i>
              </div>
              <div class="tp-caption largeblackbg sfb"
                data-x="682"
                data-y="250"
                data-speed="500"
                data-start="2200"
                data-easing="Back.easeOut"
                data-endspeed="500"
                data-endeasing="Power4.easeIn"
                data-captionhidden="on"
                style="z-index: 9">
		<i class="icon-desktop"></i>
<!--<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/logo-slider.png" alt="">-->
              </div>
              <div class="tp-caption largeblackbg sfb"
                data-x="765"
                data-y="250"
                data-speed="500"
                data-start="2400"
                data-easing="Back.easeOut"
                data-endspeed="500"
                data-endeasing="Power4.easeIn"
                data-captionhidden="on"
                style="z-index: 9">=
              </div>
              <div class="tp-caption largeblackbg sfb"
                data-x="815"
                data-y="250"
                data-speed="500"
                data-start="2600"
                data-easing="Back.easeOut"
                data-endspeed="500"
                data-endeasing="Power4.easeIn"
                data-captionhidden="on"
                style="z-index: 9"><i class="icon-heart"></i>
              </div>
              <div class="tp-caption largebutton lft"
                data-x="550"
                data-y="350"
                data-speed="500"
                data-start="2800"
                data-easing="Back.easeOut"
                data-endspeed="500"
                data-endeasing="Power4.easeIn"
                data-captionhidden="on"
                style="z-index: 9"><a href="<?php home_url(); ?>/website-portfolio-groningen">Bekijk voorbeelden</a>
              </div>
            </div>
          </li>