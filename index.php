<?php
/*
	Template Name: Main
*/

	$OveronsID = 4193;
?>

<?php get_header(); ?>
  
      <div class="tp-banner-container" style="height:500px;">
        <div class="tp-banner" >
          <ul style="display:none;">
           
				  <?php
					get_template_part( 'content', 'banner' );
				?>      
        </ul>
      </div>
      <script type="text/javascript">
        var revapi;
        jQuery(document).ready(function() {
             revapi = jQuery('.tp-banner').revolution({
              delay:9000,
              startwidth:1170,
              startheight:500,
              hideThumbs:10,
              forceFullWidth:"off",
            });
        }); //ready
      </script>
      <div class="container marg75">
        <div class="row">
          <div class="col-lg-12">
            <div class="promo-block">
              <div class="promo-text">Diensten</div>
              <div class="center-line"></div>
            </div>
          </div>
        </div>
      </div>

      <div class="container animated-area">
        <div class="row">
          <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 col-ms-12 animated" data-animation-delay="0.3s" data-animation="fadeInUp">
            <div class="hi-icon-effect marg50">
              <div class="hi-icon icon-lightbulb"></div>
              <div class="service-name">Websitefotografie</div>
              <div class="service-text">Een website komt pas echt tot leven met prachtige professionele fotografie. Een foto met je eigen producten, bedrijf of mensen geeft net dat beetje extra.</div>
            </div>
          </div>
          <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 col-ms-12 animated" data-animation-delay="0.6s" data-animation="fadeInUp">
            <div class="hi-icon-effect marg50">
              <div class="hi-icon icon-heart"></div>
              <div class="service-name">Website design</div>
              <div class="service-text">Elke site wordt ontworpen vanuit de gebruiker: functie gaat voor vorm, je klanten moeten tenslotte vinden wat ze zoeken. Uiteraard moet het eindresultaa er wel oogstrelend uitzien!</div>
           </div>
          </div>
          <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 col-ms-12 animated" data-animation-delay="0.9s" data-animation="fadeInUp">
            <div class="hi-icon-effect marg50">
              <div class="hi-icon icon-params"></div>
              <div class="service-name">Webdevelopment</div>
              <div class="service-text">Ontwikkeling volgens de nieuwste standaarden, HTML5, CSS3, JQuery, PHP, op platvormen Wordpress of Drupal.</div>
           </div>
          </div>
          <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 col-ms-12 animated" data-animation-delay="1.2s" data-animation="fadeInUp">
            <div class="hi-icon-effect marg50">
              <div class="hi-icon icon-globe"></div>
              <div class="service-name">SEO</div>
              <div class="service-text">Elke site wordt geoptimaliseerd voor search engines afgeleverd, maar ook denken we mee over je domeinnaam en social media. Elke succesvolle site is voor ons een visitekaartje!</div>
            </div>
          </div>
        </div>
      </div>


      <div class="container-color marg75">
        <div class="container">
          <div class="row">
            <div class="col-lg-12">
              <div class="promo-block">
                <div class="promo-text">Recent werk</div>
                <div class="center-line"></div>
              </div>
              <div class="promo-paragraph">Je gaat natuurlijk niet met ons in zee zonder dat je weet wat we voor je kunnen betekenen. Bekijk hier onze meest recente projecten. </div>
            </div>
          </div>
        </div>

        <div class="container marg50 animated-area">
          <div class="grid hover-3">
            <div class="cbp-l-grid-projects" id="grid-container">
              <ul>

<?php

$category_id = get_cat_ID('portfolio');
$websitefotografie_tag = array(get_tag_ID('websitefotografie'));
$args = array(
	'post_type' => 'post',
	'cat' => $category_id,
	'tag__not_in' =>  $websitefotografie_tag
);

$the_query = new WP_Query( $args );

if ( $the_query->have_posts() ) {
	$count=0;
	while ( $the_query->have_posts() ) {
		$the_query->the_post();
		?>

		<li class="cbp-item animated" data-animation-delay="<?php echo 0.3+($count*0.2);?>s" data-animation="fadeIn"">
                  <div class="portfolio-main">
                    <figure>
                     <?php the_post_thumbnail('portfolio-image');?>
                      <figcaption>
                        <h3><?php the_title(); ?></h3>
                    
                        <a href="<?php 
$thumb = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'portfolio-image-big' );
echo $thumb['0']; ?>" class="portfolio-attach cbp-lightbox" data-title="" alt="lees meer"><i class="icon-search"></i></a>
                        <a href="<?php echo get_the_content();?>" class="portfolio-search" alt="ga naar website"><i class="icon-attach"></i></a>
                      </figcaption>
                    </figure> 
                  </div>
                </li>


		<?php
		$count++;
	}
 	
} else {
	// no posts found
}
/* Restore original Post Data */
wp_reset_postdata();
				   
?>				

               </ul>
            </div>
<!--
            <div class="col-lg-12">
              <div class="button-center"><a href="portfolio/loadmore.html" class="btn-simple cbp-l-loadMore-button-link">Load Full Portfolio</a></div>
            </div>
-->
          </div>  
        </div>
      </div>
      <div class="prl-1">
        <div class="prlx">
          <div class="container">
            <div class="row">
              <div class="col-lg-12 marg50"><div class="quote"><i class="fa fa-quote-right"></i></div></div>
              <div class="col-lg-12">
                <div class="testimonials">
                  <div id="carousel-testimonials" class="carousel slide">
                    <ol class="carousel-indicators">
                      <li data-target="#carousel-testimonials" data-slide-to="0" class="active"></li>
                      <li data-target="#carousel-testimonials" data-slide-to="1" class=""></li>
                      <li data-target="#carousel-testimonials" data-slide-to="2" class=""></li>
                      <li data-target="#carousel-testimonials" data-slide-to="3" class=""></li>
                    </ol>
                    <div class="carousel-inner">
                      <div class="item active">
                        <p class="testimonial-quote">Ik ben nog steeds erg blij met mijn site. De site is  overzichtelijk, en straalt kwaliteit uit. </p>
                        <p class="testimonial-author">Byssus Zonnepanelen montage</p>
                      </div>
                      <div class="item">
                        <p class="testimonial-quote">Rogier Lankhorst is technisch heel goed onderlegd, en heeft ook veel oog voor de esthetiek van de website. Bovendien is hij pro-actief: Hij denkt vanuit zichzelf mee over mogelijkheden.</p>
                        <p class="testimonial-author">A.J. Smits, Denkbeeld.info</p>
                      </div>
                      <div class="item">
                        <p class="testimonial-quote">Goede nazorg!</p>
                        <p class="testimonial-author">Fotojournalist Geert Job Sevink</p>
                      </div>
                    </div>
                    <a class="left carousel-control" href="#carousel-testimonials" data-slide="prev">&lsaquo;</a>
                    <a class="right carousel-control" href="#carousel-testimonials" data-slide="next">&rsaquo;</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="container marg75 animated-area">
        <div class="row">
          <div class="col-lg-6 col-md-6 col-sm-12 animated" data-animation-delay="0.3s" data-animation="fadeInLeft">

<?php
$args = array(
	'post_type' => 'page',
	'page_id' => $OveronsID
);

$the_query = new WP_Query( $args );

if ( $the_query->have_posts() ) {
        
	while ( $the_query->have_posts() ) {
		$the_query->the_post();
		?>
		<div class="promo-block">
              <div class="promo-text"><?php echo get_the_title(); ?></div>
              <div class="center-line"></div>
            </div>
            <div class="marg50">
              <p class="about-text">
		<?php echo get_custom_content();?></p>
              
            </div><!--marg 50-->
          </div><!--promo block-->

		<?php
	}
 
} else {
	// no posts found
}
/* Restore original Post Data */
wp_reset_postdata();
				   
?>			
            
          <div class="col-lg-6 col-md-6 col-sm-12 animated" data-animation-delay="0.3s" data-animation="fadeInRight">
            <div class="promo-block">
              <div class="promo-text">Laatste berichten</div>
              <div class="center-line"></div>
            </div>
            <div class="row marg50">
<?php
$category_id = get_cat_ID('blog');
$args = array(
	'post_type' => 'post',
	'cat' => $category_id,
	'posts_per_page' => 2
);

$the_query = new WP_Query( $args );

if ( $the_query->have_posts() ) {
	while ( $the_query->have_posts() ) {
		$the_query->the_post();
		?>
<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 col-ms-12">
                <div class="blog-main">
                  <div class="blog-images">
                    <div class="view view-fifth">
                      <?php the_post_thumbnail('blog-image-homepage');?>
                      <div class="mask"><a href="#" class="btn-blog">Lees meer</a></div>
                    </div>
                  </div>
                  <div class="blog-icon"><i class="icon-pencil"></i></div>
                  <div class="blog-name"><a href="<?php the_permalink();?>"><?php the_title();?></a></div>
                  <div class="blog-desc"><?php the_date();?></div>
                </div>
              </div>

		<?php
	}
 	
} else {
	// no posts found
}
/* Restore original Post Data */
wp_reset_postdata();
				   
?>
            </div>
          </div>
        </div>
      </div>
      <div class="container marg75">
        <div class="row">
          <div class="col-lg-12">
            <div class="promo-block">
              <div class="promo-text">Onze klanten<a id="klanten">&nbsp;</a></div>
              <div class="center-line"></div>
            </div>
          </div>
          <div class="col-lg-12 marg25">
            <div class="jcarousel-wrapper">
              <div class="jcarousel animated-area">
                <ul>
<?php
$category_id = get_cat_ID('klanten');
$args = array(
	'post_type' => 'post',
	'cat' => $category_id
);

$the_query = new WP_Query( $args );

if ( $the_query->have_posts() ) {
	$count=0;
	while ( $the_query->have_posts() ) {
		$the_query->the_post();
		
		?>
		<li class="animated" data-animation-delay="<?php echo 0.3+($count*0.2);?>s" data-animation="fadeIn<?php echo ($count % 2 == 0 ? "Up" : "Down")?>">
			<a target="_blank" href="<?php echo get_the_content();?>">
				<?php the_post_thumbnail('klanten-image');?>
			</a>
		</li>
		<?php
		$count++;
	}
 	
} else {
	// no posts found
}
/* Restore original Post Data */
wp_reset_postdata();
				   
?>	
         
                </ul>
              </div>
              <a href="#" class="jcarousel-control-prev">&lsaquo;</a>
              <a href="#" class="jcarousel-control-next">&rsaquo;</a>
            </div>
          </div>
        </div>
      </div>
     <?php get_footer(); ?>