<?php get_header(); ?>
<div class="page-in">
        <div class="container">
          <div class="row">
            <div class="col-lg-6 pull-left"><div class="page-in-name">Zoekresultaten: <span><?php echo get_search_query(); ?></span></div></div>
            <div class="col-lg-6 pull-right"><div class="page-in-bread"><div class="page-in-bread"><?php if ( function_exists('yoast_breadcrumb') ) {
yoast_breadcrumb('<p id="breadcrumbs">','</p>');
} ?></div></div>
          </div>
        </div>
      </div>
      <div class="container marg50">
        <div class="row">
          <div class="col-lg-12">
            <div class="row">

 <?php
    global $wp_query;
    $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
     
     $args = array_merge( $wp_query->query_vars, array('paged' => $paged ) );
     $wp_query = new WP_Query( $args );

 if (have_posts()) : ?>

            <?php while (have_posts()) : the_post(); ?>

         
              <div class="medium-blog">
                <div class="col-lg-5">
                  <div class="cl-blog-img">
		<?php
		if ( has_post_thumbnail() ) {
			the_post_thumbnail('list-page-image');
		} else {
			echo '<img src="'. get_bloginfo( 'stylesheet_directory' ) . '/images/mockup.jpg" />';
		}
		?>
		</div>
                </div>
                <div class="col-lg-7">
                  <div class="med-blog-naz">
                    <div class="cl-blog-type"><i class="icon-pencil"></i></div>
                    <div class="cl-blog-name"><a href="#"><?php the_title();?></a></div>
                    <div class="cl-blog-text"><?php the_content();?> </div>
                  </div>
                  <div class="cl-blog-read"><a href="<?php the_permalink() ?>">Lees verder</a></div>
                </div>
                <div class="cl-blog-line"></div>
              </div><!-- medium blog -->
            
            <?php endwhile; ?>

            

            <?php else : ?>

	<div class="medium-blog">
                <div class="col-lg-5">
                  <div class="cl-blog-img"><img src="assets/images/large_44.jpg" alt=""></div>
                </div>
                <div class="col-lg-7">
                  <div class="med-blog-naz">
                    <div class="cl-blog-type"><i class="icon-pencil"></i></div>
                    <div class="cl-blog-name"><a href="#"><?php the_title();?>Niets gevonden...</a></div>
                    <div class="cl-blog-text">Helaas, er zijn geen resultaten die voldoen aan je zoekopdracht. Probeer het nog eens! </div>
                  </div>
              
                </div>
                <div class="cl-blog-line"></div>
              </div><!-- medium blog -->

              


                    <?php get_search_form(); ?>
          
      
            <?php endif; ?>

            
            </div><!-- row -->
            <div class="row">
              <div class="col-lg-12">

                <div class="pride_pg">
                  <?php
$big = 999999999; // need an unlikely integer
echo paginate_links( array(
  'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
  'format' => '?paged=%#%',
  'current' => max( 1, get_query_var('paged') ),
  'total' => $wp_query->max_num_pages,
  'type'  => 'list'
) );
?>
                </div>

              </div>

            </div>
          </div> 
        </div>
      </div>

        

<?php get_footer(); ?>        