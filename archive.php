<?php 
/*
	Template Name: Tag archive
*/

get_header(); ?>
  <!--archive-->
          <div class="page-in">
        <div class="container">
          <div class="row">
            <div class="col-lg-6 pull-left"><div class="page-in-name">Label: <span><?php single_tag_title(); ?></span></div></div>
            <div class="col-lg-6 pull-right"><div class="page-in-bread"><?php if ( function_exists('yoast_breadcrumb') ) {
yoast_breadcrumb('<p id="breadcrumbs">','</p>');
} ?></div></div>
          </div>
        </div>
      </div>
      <div class="container marg50">
        <div class="row">
          <div class="col-lg-12">
            <div class="row">

<?php 
    global $wp_query;
    $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
     
     $args = array_merge( $wp_query->query_vars, array('paged' => $paged ) );
     $wp_query = new WP_Query( $args );
    if(have_posts()) : while(have_posts()) : the_post();
?>


<div class="medium-blog">
                <div class="col-lg-5">
                  <div class="cl-blog-img"><?php the_post_thumbnail('blog-image');?></div>
                </div>
                <div class="col-lg-7">
                  <div class="med-blog-naz">
                    <div class="cl-blog-type"><i class="icon-pencil"></i></div>
                    <div class="cl-blog-name"><?php the_title();?></div>
                    <div class="cl-blog-detail"><?php the_date()?></div>
                    <div class="cl-blog-text">
                      <?php
                       if (in_category("Blog")) {
                           echo get_custom_content(TRUE);
                      }
                      else
                      {
                        ?>
                          <a href="<?php the_content();?>"><?php the_content();?></a>
                        <?php
                      }
                      ?>

                    </div>
                  </div>
                  <?php
                  if (in_category("Blog")) {
                    ?>
                      <a href="<?php the_permalink();?>"><div class="cl-blog-read">Lees meer</div></a>
                    <?php } ?>
                </div>
                <div class="cl-blog-line"></div>
              </div>
	<?php endwhile; endif;?>
  
        
 <?php
/* Restore original Post Data */
wp_reset_postdata();
				   
?>


              
            <div class="row">
              <div class="col-lg-12">
                <div class="pride_pg">

<?php
$big = 999999999; // need an unlikely integer
echo paginate_links( array(
	'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
	'format' => '?paged=%#%',
	'current' => max( 1, get_query_var('paged') ),
	'total' => $wp_query->max_num_pages,
	'type'  => 'list'
) );
?>
                  
                </div>
              </div>
            </div>
          </div> 
        </div>
      </div>
</div>
<?php get_footer();?>