<?php 
/*
	Template Name: Blog
*/

get_header(); ?>
  
          <div class="page-in">
        <div class="container">
          <div class="row">
            <div class="col-lg-6 pull-left"><div class="page-in-name"><?php the_title(); ?>: <span><?php echo get_post_meta(get_the_ID(), 'ex_ondertitel', true);?></span></div></div>
            <div class="col-lg-6 pull-right"><div class="page-in-bread"><?php if ( function_exists('yoast_breadcrumb') ) {
yoast_breadcrumb('<p id="breadcrumbs">','</p>');
} ?></div></div>
          
        </div>
      </div><!--container -->
      <div class="container marg50">
        <div class="row" >
          <div class="col-lg-12" >
            <div class="row" >
<?php
$category_id = get_cat_ID('blog');
$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;

$args = array(
	'paged' => $paged,
	'post_type' => 'post',
	'cat' => $category_id
);

$the_query = new WP_Query( $args );

if ( $the_query->have_posts() ) {
	while ( $the_query->have_posts() ) {
		$the_query->the_post();
		?>

    <div class="medium-blog">
          <div class="col-lg-12">
            <div class="med-blog-naz">
              <div class="cl-blog-type"><i class="icon-pencil"></i></div>
              <div class="cl-blog-name"><?php the_title();?></div>

              <div class="cl-blog-detail"><?php the_date()?> - <a href="https://plus.google.com/u/0/117371587720581418238?rel=author">Rogier Lankhorst</a> <div class="addthis_sharing_toolbox"></div></div>
              <div class="cl-blog-text"><?php echo get_custom_content(true);?></div>
            </div>
            <div class="cl-blog-read"><a href="<?php the_permalink();?>">Lees meer</a></div>
          </div>
          <div class="cl-blog-line" style="float:left;"></div>
    </div><!--medium blog-->
    <div class="clear"></div>

		<?php
	}
 	
} else {
	// no posts found
}
/* Restore original Post Data */
wp_reset_postdata();
				   
?>


              
            <div class="row">
              <div class="col-lg-12">

                <div class="pride_pg">
                  <?php
$big = 999999999; // need an unlikely integer

echo paginate_links( array(
	'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
	'format' => '?paged=%#%',
	'current' => max( 1, get_query_var('paged') ),
	'total' => $the_query->max_num_pages,
	'type'  => 'list'
) );
?>
                </div>

              </div>
            </div>


          </div> <!--row-->
        </div>
      </div><!--row-->
</div><!--container marg 50-->

</div><!--page in-->
<?php get_footer();?>