<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-538c6149159b7b7f"></script>

<div class="prl-3 marg50 twitter-footer">
        <div class="prlx-3">
          <div class="container marg75">
            <div class="row">
              <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 col-ms-12">
                <div class="twit-icon"><i class="fa fa-twitter"></i></div>
              </div>
              <div class="col-lg-10 col-md-10 col-sm-10 col-xs-11 col-ms-12">
                <div class="tweet"></div>
                
              </div>
              <div class="col-lg-1 col-md-1 col-sm-1 hidden-xs">
                <div class="paginat">
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="footer" style="margin-top:0px;">
        <div class="container">
          <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-4">
              <div class="promo-text-footer">Adres</div>
              <ul class="contact-footer">
                <li><i class="icon-location"></i> Adres: Eridanusstraat 16, Groningen</li>
                <li><i class="icon-mobile"></i> Telefoon: 063747 2437</li>
               <!-- <li><i class="icon-videocam"></i> Skype: companyname</li>-->
                <li><i class="icon-mail"></i> E-mail: <?php include 'encoded-email.php';?></li>
                <li><i class="icon-key"></i> Flexwerkplek: Seats2meet Groningen</li>
              </ul> 
               <ul class="soc-footer">
                <li><a target="_blank" href="https://www.linkedin.com/in/rogierlankhorst"><i class="fa fa-linkedin-square"></i></a></li>
                <li><a target="_blank" href="https://plus.google.com/u/0/117371587720581418238/posts"><i class="fa fa-google-plus-square"></i></a></li>
              </ul>       
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4">
                            <div class="promo-text-footer">Populair</div>
                <ul class="tags-footer">
                  <li><?php wp_tag_cloud(); ?></li>
                 
                </ul>

              </div>



   
            <div class="col-lg-4 col-md-4 col-sm-4">

          </div>
        </div>
        <div class="container">
          <div class="footer-bottom">
            <div class="row">
              <div class="col-lg-6 col-md-6 col-sm-6 col-ms-12 pull-left">
                <div class="copyright">Copyright © 2014 Rogier Lankhorst <a href="http://www.rogierlankhorst.com" target="_blank">rogierlankhorst.com</a></div>
              </div>
              <div class="col-lg-6 col-md-6 col-sm-6 col-ms-12 pull-right">
                <div class="foot_menu">
                  <ul>
                    <li><a href="<?php echo esc_url( home_url( '/' ) ); ?>">Home</a></li>
                    <li><a href="<?php echo esc_url( home_url( '/' ) ); ?>/over">Over</a></li>
                    <li><a href="<?php echo esc_url( home_url( '/' ) ); ?>/blog">Blog</a></li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>  
<script>

  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-43617998-1', 'rogierlankhorst.com');
  ga('require', 'displayfeatures');
  ga('send', 'pageview');

</script>

<script data-cfasync="true" type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/assets/js/modernizr.custom.js"></script>
    <script data-cfasync="true" type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/assets/rs-plugin/js/jquery.themepunch.plugins.min.js"></script>
    <script data-cfasync="true" type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/assets/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
    <script data-cfasync="true" type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/assets/js/waypoints.min.js"></script>
    <script data-cfasync="true" type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/assets/js/sticky.js"></script>
    <script data-cfasync="true" type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/assets/js/bootstrap.min.js"></script>
    <script data-cfasync="true" type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/assets/js/jquery.flexslider-min.js"></script>
    <script data-cfasync="true" type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/assets/js/jquery.cycle.all.js"></script>
    <script data-cfasync="true" type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/assets/js/jquery.parallax-1.1.3.js"></script>
    <script data-cfasync="true" type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/assets/js/jquery.cubeportfolio.min.js"></script>
    <script data-cfasync="true" type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/assets/js/portfolio-main.js"></script>
    <script data-cfasync="true" type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/assets/js/jcarousel.responsive.js"></script>
    <script data-cfasync="true" type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/assets/js/jquery.jcarousel.min.js"></script>
    <script data-cfasync="true" type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/assets/js/jquery.dlmenu.js"></script>
    <script data-cfasync="true" type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/assets/js/main.js"></script>

  </body>
</html>
<?php wp_footer(); ?>