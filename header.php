<!DOCTYPE html>
<html lang="nl" class="no-js" > <!--manifest="/offline.appcache" type="text/cache-manifest">-->
  <head>

    <!--[if IE]><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"><![endif]-->
    
    <meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1" name="viewport"/>
    <link href="<?php bloginfo('stylesheet_directory'); ?>/images/favicon.ico" rel="shortcut icon"/>
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php bloginfo('stylesheet_directory'); ?>/assets/images/apple-touch-icon-144x144-precomposed.png" />
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php bloginfo('stylesheet_directory'); ?>/assets/images/apple-touch-icon-114x114-precomposed.png" />
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php bloginfo('stylesheet_directory'); ?>/assets/images/apple-touch-icon-72x72-precomposed.png" />
    <link rel="apple-touch-icon-precomposed" href="<?php bloginfo('stylesheet_directory'); ?>/assets/images/apple-touch-icon-precomposed.png" />

    <!-- CSS FILES -->
    <link href="<?php bloginfo( 'stylesheet_url' ); ?>" media="screen" rel="stylesheet" type="text/css">
    <link href="<?php bloginfo('stylesheet_directory'); ?>/assets/css/responsive.css" media="screen" rel="stylesheet" type="text/css">
    <link href="<?php bloginfo('stylesheet_directory'); ?>/assets/rs-plugin/css/settings.css" media="screen" rel="stylesheet" type="text/css">
    <link href="<?php bloginfo('stylesheet_directory'); ?>/assets/css/navstylechange.css" media="screen" rel="stylesheet" type="text/css">
    <link href="<?php bloginfo('stylesheet_directory'); ?>/assets/css/cubeportfolio.min.css" media="screen" rel="stylesheet" type="text/css">
<link href="<?php bloginfo('stylesheet_directory'); ?>/assets/css/layouts/green.css" media="screen" rel="stylesheet" type="text/css">
<?php wp_head(); ?>
  </head>

<body>
    <div id="wrapper">
      <header>
        <div class="top_line">
          <div class="container">
            <div class="row">
              <div class="col-lg-6 col-md-7 col-sm-12 col-xs-12 pull-left">
                <ul class="contact-top">
                  <li><i class="icon-location"></i> Eridanusstraat 16, Groningen</li>
                  <li><i class="icon-mobile"></i> 06 3747 2437</li>
                  <li><i class="icon-mail"></i> <?php include 'encoded-email.php';?></li>
                </ul>
              </div>
              <div class="col-lg-6 col-md-5 pull-right hidden-phone">
                  <ul class="social-links">
                    <li><a href="https://www.facebook.com/rogierlankhorstwebsites"><i class="fa fa-facebook"></i></a></li>
                    <li><a target="_blank" href="https://www.linkedin.com/in/rogierlankhorst/"><i class="fa fa-linkedin"></i></a></li>
                    <li><a target="_blank" href="https://plus.google.com/102832182977969112784/about?hl=nl"><i class="fa fa-google-plus"></i></a></li>
                    <li id="search-btn"><a href="#"><i class="icon-search"></i></a></li>
                  </ul>

		<?php include 'searchform.php'; ?>
              </div> 
            </div>
          </div>
        </div>
      </header>
      <div class="page_head">
        <div class="nav-container" style="height: auto;">
          <nav>
            <div class="container">
              <div class="row">
                <div class="col-lg-3 pull-left"><div class="logo">
<a href="<?php echo esc_url( home_url( '/' ) ); ?>">
<img src="<?php bloginfo('stylesheet_directory'); ?>/images/rogierlankhorst-logo-dtc-s.png" alt=""></a></div></div>
                <div class="col-lg-9 pull-right">
                  <div class="menu">
                    <div id="dl-menu" class="dl-menuwrapper">
                    <button class="dl-trigger">Open Menu</button>
  <!-- nav menu here-->
<?php 
$defaults = array(
        'container'       =>  false,
        'theme_location'  => 'main_menu',
        'menu_class'      => 'dl-menu',
        'walker'        => new My_Sub_Menu()
    );
	wp_nav_menu($defaults); ?>          
                    </div> 
                  </div>        
                </div>
              </div>
            </div>
          </nav>
        </div></div>